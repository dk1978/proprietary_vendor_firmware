FIRMWARE_IMAGES := \
    abl \
    aop \
    bluetooth \
    cpucp \
    devcfg \
    dsp \
    hyp \
    keymaster \
    logo \
    modem \
    prov \
    qupfw \
    shrm \
    storsec \
    tz \
    uefisecapp \
    xbl_config \
    xbl

AB_OTA_PARTITIONS += $(FIRMWARE_IMAGES)
